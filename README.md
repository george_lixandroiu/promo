# HAPI - Scaffold API

Use this scaffold project when starting a new data API.
Steps to start a new project
```
git clone url-to-this-repo.git my_project
cd my_project
npm i
```
Then you need to rename your package.json accordingly -> scaffold-api becomes my-project-api.
Replace the repo with your new project's repo:
```
git remote add origin https://bitbucket-tools.altex.ro/scm/ams/hapi-my-project.git
git push origin master
```

It's time now to be hapi!
