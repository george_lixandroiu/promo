'use strict';

import GeneralValidation from '../validate/general';
import CategoryValidation from '../validate/category';
import Helper from '../lib/helper';
import ModelHelper from '../lib/modelHelper';
import Joi from '@hapi/joi';
import Boom from '@hapi/boom';
import _ from 'lodash';

module.exports = (settings) => ({
    save: {
        handler: async (request, reply) => {

            const pandaEsOrm = request.server.pandaEsOrm;
            const categoryModel = pandaEsOrm.getModel('CategoryModel');
            const campaignModel = pandaEsOrm.getModel('CampaignModel');
            const category = request.payload;

            try {
                const campaign = await campaignModel.findOneById(category.campaign_id);
                if (_.isEmpty(campaign.data)) {
                    return Boom.notFound('Campaign doesn\'t exist');
                }

                if (!_.isUndefined(category.parent_id)) {
                    const parentCategory = await categoryModel.findOneById(category.parent_id);
                    if (_.isEmpty(parentCategory.data)) {
                        return Boom.notFound('Parent Category doesn\'t exist');
                    }
                }

                //insert category
                categoryModel.data = category;
                await categoryModel.create();

                return { message: 'Saved' };
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        auth: 'im-auth',
        validate: {
            payload: Joi.object().label('Category').keys(CategoryValidation.getValidationSchema())
        },
        description: 'Update Category',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'ADD'
                }
            }
        }
    },
    update: {
        handler: async (request, reply) => {

            const pandaEsOrm = request.server.pandaEsOrm;
            const categoryModel = pandaEsOrm.getModel('CategoryModel');
            const campaignModel = pandaEsOrm.getModel('CampaignModel');
            const category = request.payload;

            try {
                const existingCategory = await categoryModel.findOneById(category.id);
                if (_.isEmpty(existingCategory.data)) {
                    return Boom.notFound('Category doesn\'t exist');
                }

                const campaign = await campaignModel.findOneById(category.campaign_id);
                if (_.isEmpty(campaign.data)) {
                    return Boom.notFound('Campaign doesn\'t exist');
                }

                if (!_.isUndefined(category.parent_id)) {
                    const parentCategory = await categoryModel.findOneById(category.parent_id);
                    if (_.isEmpty(parentCategory.data)) {
                        return Boom.notFound('Parent Category doesn\'t exist');
                    }
                }

                //delete category
                categoryModel.data = { id: category.id };
                await categoryModel.delete();

                //insert category
                categoryModel.data = category;
                await categoryModel.create();

                return { message: 'Updated' };
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        auth: 'im-auth',
        validate: {
            payload: Joi.object().label('Category').keys(CategoryValidation.getValidationSchema())
        },
        description: 'Update Category',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'EDIT'
                }
            }
        }
    },
    getByIdAndAttachAssociatedProducts: {
        handler: async (request, reply) => {

            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const categoryModel = pandaEsOrm.getModel('CategoryModel');
            const campaignModel = pandaEsOrm.getModel('CampaignModel');
            const productModel = pandaEsOrm.getModel('ProductModel');

            try {
                let categoryConditions = ModelHelper.getPredefinedFiltersForModel('categoryModel');
                categoryConditions = categoryConditions.concat({ match: { id: request.params.id } });

                const category = (await categoryModel.findOne(new pandaEs.query({
                    query: {
                        bool: {
                            must: categoryConditions
                        }
                    }
                }))).data;
                if (_.isEmpty(category)) {
                    return Boom.notFound();
                }

                let productConditions = [
                    {
                        terms: {
                            entity_id: category.product_ids
                        }
                    }
                ];
                const mustNotFilters = [];

                const campaign = await campaignModel.findOne(new pandaEs.query({
                    query: {
                        bool: {
                            must: {
                                match: {
                                    id: category.campaign_id
                                }
                            }
                        }
                    }
                }));
                if (!ModelHelper.campaignIsActive(campaign.data)) {
                    return Boom.notFound('Associated campaign could not be found')
                }

                //process campaign flags
                const displayOutOfStock = _.get(campaign, ['data', 'properties', 'display_out_of_stock'], false);
                const displayNoDiscounts = _.get(campaign, ['data', 'properties', 'display_no_discounts'], false);
                let specialSku = _.get(campaign, ['data', 'properties', 'special_price_label_sku'], []);

                if (_.isString(specialSku) && specialSku !== "") {
                    specialSku = specialSku.split(',');
                }

                if (!displayOutOfStock) { //get only in stock products
                    mustNotFilters.push({
                        terms: {
                            stock_status: [0, 4] // OOS& EOL
                        }
                    });
                } else {
                    mustNotFilters.push({
                        term: {
                            stock_status: 4 // EOL
                        }
                    });
                }

                //process custom filters
                if (!_.isEmpty(request.query) && !_.isEmpty(request.query.filter)) {
                    let filters = request.query.filter;
                    if (!_.isArray(filters)) {
                        filters = [filters];
                    }

                    productConditions = productConditions.concat((Helper.processFilters(filters)).conditions);
                }

                //attach products to category object
                if (!_.isUndefined(category.product_ids)) {
                    let products = (await productModel.find(new pandaEs.query({
                        query: {
                            bool: {
                                must: productConditions,
                                must_not: mustNotFilters
                            }
                        },
                        size: category.product_ids.length
                    }))).map((obj) => obj._data);

                    const sortedProducts = [];
                    for (const position in category.product_ids) {
                        const productToFind = category.product_ids[position];

                        for (let key in products) {
                            const product = products[key];

                            if (parseInt(productToFind) === product.entity_id) {
                                if (_.includes(specialSku, product.sku)) { //flag special label products
                                    product.special_price_label = true;
                                }
                                else if (!displayNoDiscounts && product.discount_type === 'none') { //we don't return this product
                                    continue;
                                }
                                sortedProducts[position] = product;
                            }
                        }
                    }

                    category.products = sortedProducts.filter(value => value !== '');
                }

                return category;
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        validate: {
            params: {
                id: Joi.number().required()
            },
            query: Joi.object().keys({
                filter: Joi.alternatives().try([
                    GeneralValidation.getFiltersValidationSchema(settings.products.allowedFilters),
                    Joi.array().items(GeneralValidation.getFiltersValidationSchema(settings.products.allowedFilters))
                ]).optional()
            })
        },
        response: {
            schema: Joi.object().keys(CategoryValidation.getValidationSchemaForResponseWithProducts()),
            options: {
                allowUnknown: true
            }
        },
        description: 'Get Category by id',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '404': {
                        'description': 'Not Found'
                    },
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            }
        }
    },
    deleteCategoryAndAssociatedProducts: {
        handler: async (request, reply) => {

            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const categoryModel = pandaEsOrm.getModel('CategoryModel');
            const productModel = pandaEsOrm.getModel('ProductModel');

            try {
                const existingCategory = (await categoryModel.findOneById(request.params.id)).data;
                if (_.isEmpty(existingCategory)) {
                    return Boom.notFound('Category doesn\'t exist');
                }

                //delete products
                if (!_.isUndefined(existingCategory.product_ids)) {
                    await pandaEs.query.execute(pandaEsOrm.connection, 'deleteByQuery', {
                        index: productModel.index,
                        body: {
                            query: {
                                bool: {
                                    must: [
                                        {
                                            terms: {
                                                id: existingCategory.product_ids
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    });
                }

                //delete category
                categoryModel.data = { id: request.params.id };
                await categoryModel.delete();

                return { message: 'Deleted' };
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        auth: 'im-auth',
        validate: {
            params: {
                id: Joi.number().required()
            }
        },
        description: 'Delete Category by id and associated Products',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'DELETE'
                }
            }
        }
    },
    deleteBulk: {
        handler: async (request, reply) => {
            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const categoriesId = request.payload.map((itm) => itm.id);
            const categoryModel = pandaEsOrm.getModel('CategoryModel');

            try {
                //delete categories
                await pandaEs.query.execute(pandaEsOrm.connection, 'deleteByQuery', {
                    index: categoryModel.index,
                    body: {
                        query: {
                            constant_score: {
                                filter: {
                                    terms: {
                                        id: categoriesId
                                    }
                                }
                            }
                        }
                    }
                });

                return { message: 'Deleted' };
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        auth: 'im-auth',
        validate: {
            payload: Joi.array().items(
                Joi.object().keys({
                    id: Joi.number().required()
                })
            )
        },
        description: 'Bulk delete categories',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'DELETE'
                }
            }
        }
    }
});
