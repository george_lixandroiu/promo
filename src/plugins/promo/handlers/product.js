'use strict';

import ProductValidation from '../validate/product';
import GeneralValidation from '../validate/general';
import Joi from '@hapi/joi';
import Boom from '@hapi/boom';
import _ from'lodash';
import Helper from '../lib/helper';

module.exports = (settings) => ({
    getBySku: {
        handler: async (request, reply) => {

            const pandaEsOrm = request.server.pandaEsOrm;
            const productModel = pandaEsOrm.getModel('ProductModel');

            try {
                const response = await productModel.findOneById(request.params.sku);
                if (_.isEmpty(response.data)) {
                    return Boom.notFound();
                }

                return response.data;
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        validate: {
            params: {
                sku: Joi.string().required()
            }
        },
        response: {
            schema: ProductValidation.getValidationSchema(),
            options: {
                allowUnknown: true
            }
        },
        description: 'Get Product by sku',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '404': {
                        'description': 'Not Found'
                    }
                }
            }
        }
    },
    getByFilters: {
        handler: async (request, reply) => {

            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const productModel = pandaEsOrm.getModel('ProductModel');

            try {
                //process filters
                let filters = request.query.filter;
                if (!_.isArray(filters)) {
                    filters = [filters];
                }

                const conditions = (Helper.processFilters(filters)).conditions;
                const response = await productModel.find(new pandaEs.query({
                    query: {
                        bool: {
                            must: conditions
                        }
                    },
                    size: 10000
                }));

                if (_.isEmpty(response)) {
                    return Boom.notFound();
                }

                return response.map((obj) => obj._data);
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        validate: {
            query: Joi.object().keys({
                filter: Joi.alternatives().try([
                    GeneralValidation.getFiltersValidationSchema(settings.products.allowedFilters),
                    Joi.array().items(GeneralValidation.getFiltersValidationSchema(settings.products.allowedFilters))
                ]).required()
            })
        },
        response: {
            schema: Joi.array().items(ProductValidation.getValidationSchema()),
            options: {
                allowUnknown: true
            }
        },
        description: 'Get Products by filters',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '404': {
                        'description': 'Not Found'
                    }
                }
            }
        }
    },
    bulkSave: {
        handler: (request, reply) => {

            const pandaEsOrm = request.server.pandaEsOrm;
            const productModel = pandaEsOrm.getModel('ProductModel');
            const connection = request.server.pandaEsOrm.connection;
            let products = request.payload;

            try {
                let index = productModel.index;
                if (!_.isUndefined(request.params.index)) {
                    index = request.params.index;
                }

                if (!_.isArray(products)) {
                    products = [products];
                }

                let batch = [];
                products.forEach(async (productData) => {

                    if (!_.has(productData, 'labels.free_shipping')) {
                        _.set(productData, 'labels.free_shipping', 0);
                    }
                    const action = { index: { _index: index, _id: productData[productModel.idKey] } };
                    batch.push(action, productData);
                });

                if (batch.length > 0) {
                    connection.bulk({
                        body: batch
                    });
                }

                return { message: 'Saved' };
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        auth: 'im-auth',
        validate: {
            params: Joi.object().keys({
                index: Joi.string().optional()
            }),
            payload: Joi.alternatives().try([
                Joi.array().label('Product').items(
                    ProductValidation.getValidationSchema()
                ),
                Joi.object().label('Product').keys(
                    ProductValidation.getValidationSchema()
                )
            ])
        },
        description: 'Save bulk products',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'ADD'
                }
            }
        }

    },
    update: {
        handler: async (request, reply) => {

            const pandaEsOrm = request.server.pandaEsOrm;
            const productModel = pandaEsOrm.getModel('ProductModel');
            const product = request.payload;

            try {
                const existingProduct = await productModel.findOneById(product.sku);
                if (_.isEmpty(existingProduct.data)) {
                    return Boom.notFound('Product doesn\'t exist');
                }

                //delete product
                productModel.data = { [productModel.idKey]: product.sku };
                await productModel.delete();

                //insert product
                productModel.data = product;
                await productModel.create();

                return { message: 'Updated' };
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        auth: 'im-auth',
        validate: {
            payload: Joi.object().label('Product').keys(ProductValidation.getValidationSchema())
        },
        description: 'Update Product',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'EDIT'
                }
            }
        }
    },
    bulkDelete: {
        handler: async (request, reply) => {

            const productIds = request.payload.map((itm) => itm.id);
            const connection = request.server.pandaEsOrm.connection;
            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const productModel = pandaEsOrm.getModel('ProductModel');

            try {
                await pandaEs.query.execute(connection, 'deleteByQuery', {
                    index: productModel.index,
                    body: {
                        query: {
                            constant_score: {
                                filter: {
                                    terms: {
                                        id: productIds
                                    }
                                }
                            }
                        }
                    }
                });

                return { message: 'Deleted' };
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        auth: 'im-auth',
        validate: {
            payload: Joi.array().items(
                Joi.object().keys({
                    id: Joi.number().required()
                })
            )
        },
        description: 'Delete bulk Products',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'DELETE'
                }
            }
        }
    }
});
