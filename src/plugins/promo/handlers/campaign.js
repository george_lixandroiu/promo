'use strict';

import CampaignValidation from '../validate/campaign';
import CategoryValidation from '../validate/category';
import GeneralValidation from '../validate/general';
import Helper from '../lib/helper';
import ModelHelper from '../lib/modelHelper';
import Joi from '@hapi/joi';
import Boom from '@hapi/boom';
import _ from 'lodash';

module.exports = (settings) => ({
    getById: {
        handler: async (request, reply) => {

            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const campaignModel = pandaEsOrm.getModel('CampaignModel');

            try {
                let conditions = ModelHelper.getPredefinedFiltersForModel('campaignModel');
                conditions = conditions.concat({ term: { id: request.params.id } });

                const response = await campaignModel.findOne(new pandaEs.query({
                    query: {
                        bool: {
                            must: conditions
                        }
                    }
                }));

                if (!ModelHelper.campaignIsActive(response.data)) {
                    return Boom.notFound();
                }

                return response.data;
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        validate: {
            params: {
                id: Joi.number().required()
            }
        },
        response: {
            schema: CampaignValidation.getValidationSchema(),
            options: {
                allowUnknown: true
            }
        },
        description: 'Get Campaign by id',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '404': {
                        'description': 'Not Found'
                    }
                }
            }
        }
    },
    getByFilters: {
        handler: async (request, reply) => {

            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const campaignModel = pandaEsOrm.getModel('CampaignModel');

            try {
                let conditions = ModelHelper.getPredefinedFiltersForModel('campaignModel');

                //process filters
                let filters = request.query.filter;
                if (!_.isArray(filters)) {
                    filters = [filters];
                }

                conditions = conditions.concat((Helper.processFilters(filters)).conditions);

                let response = await campaignModel.find(new pandaEs.query({
                    query: {
                        bool: {
                            must: conditions
                        }
                    },
                    size: 1000
                }));

                response = response.map((obj) => obj._data);

                for (const key in response) {
                    if (!ModelHelper.campaignIsActive(response[key])) {
                        delete response[key];
                    }
                }
                response = response.filter(function (obj) { return obj != null; });

                if (response.length === 0) {
                    return Boom.notFound();
                }

                return response;
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        validate: {
            query: Joi.object().keys({
                filter: Joi.alternatives().try([
                    GeneralValidation.getFiltersValidationSchema(settings.campaigns.allowedFilters),
                    Joi.array().items(GeneralValidation.getFiltersValidationSchema(settings.campaigns.allowedFilters))
                ]).required()
            })
        },
        response: {
            schema: Joi.array().items(CampaignValidation.getValidationSchema()),
            options: {
                allowUnknown: true
            }
        },
        description: 'Get Campaigns by filters',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '404': {
                        'description': 'Not Found'
                    }
                }
            }
        }
    },
    getCategoriesByCampaignIdAndCategoryFilters: {
        handler: async (request, reply) => {

            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const categoryModel = pandaEsOrm.getModel('CategoryModel');
            let stopAtLevel = settings.categories.maxLevel;

            try {
                let conditions = ModelHelper.getPredefinedFiltersForModel('categoryModel');
                conditions = conditions.concat({ term: { campaign_id: request.params.id } });

                //process filters
                if (!_.isEmpty(request.query.filter)) {
                    let filters = request.query.filter;
                    if (!_.isArray(filters)) {
                        filters = [filters];
                    }

                    const data = Helper.processFilters(filters, ['level']);
                    if (!_.isEmpty(data.level)) {
                        stopAtLevel = data.level;
                    }

                    conditions = conditions.concat(data.conditions);
                }

                const response = (await categoryModel.find(new pandaEs.query({
                    query: {
                        bool: {
                            must: conditions
                        }
                    },
                    size: 1000
                }))).map((obj) => obj._data);

                if (_.isEmpty(response)) {
                    return Boom.notFound();
                }

                response.sort((a, b) => {

                    return a.sort_order - b.sort_order;
                });

                return Helper.constructCategoriesTree(response, 1, stopAtLevel);
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        validate: {
            params: {
                id: Joi.number().required()
            },
            query: Joi.object().keys({
                filter: Joi.alternatives().try([
                    GeneralValidation.getFiltersValidationSchema(settings.categories.allowedFilters),
                    Joi.array().items(GeneralValidation.getFiltersValidationSchema(settings.categories.allowedFilters))
                ]).optional()
            })
        },
        response: {
            schema: Joi.array().items(CategoryValidation.getValidationSchemaForResponseWithMultipleLevels()),
            options: {
                allowUnknown: true
            }
        },
        description: 'Get categories by campaign id and category filters',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '404': {
                        'description': 'Not Found'
                    }
                }
            }
        }
    },
    bulkSave: {
        handler: (request, reply) => {

            const pandaEsOrm = request.server.pandaEsOrm;
            const connection = request.server.pandaEsOrm.connection;
            const campaignModel = pandaEsOrm.getModel('CampaignModel');
            const categoryModel = pandaEsOrm.getModel('CategoryModel');
            let campaigns = request.payload;

            try {
                if (!_.isArray(campaigns)) {
                    campaigns = [campaigns];
                }

                let campaignsBatch = [];
                let categoriesBatch = [];

                for (let campaignData of campaigns) {
                    if (
                        !_.isUndefined(campaignData.categories) &&
                        _.isArray(campaignData.categories) &&
                        campaignData.categories.length > 0
                    ) {
                        for (let categoryData of campaignData.categories) {
                            categoriesBatch.push({index: {_index: request.params.category_index, _id: categoryData.id}});
                            categoriesBatch.push(categoryData);

                            if (categoriesBatch.length === 100) {
                                Helper.bulkOperation(request.server, connection, categoriesBatch);
                                categoriesBatch = [];
                            }
                        }
                    }

                    delete campaignData.categories;

                    campaignsBatch.push({index: {_index: request.params.campaign_index, _id: campaignData.id}});
                    campaignsBatch.push(campaignData);

                    if (campaignsBatch.length === 50) {
                        Helper.bulkOperation(request.server, connection, campaignsBatch);
                        campaignsBatch = [];
                    }
                }

                if (campaignsBatch.length !== 0) {
                    Helper.bulkOperation(request.server, connection, campaignsBatch);
                }
                if (categoriesBatch.length !== 0) {
                    Helper.bulkOperation(request.server, connection, categoriesBatch);
                }

                return { message: 'Saved' };
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        auth: 'im-auth',
        validate: {
            params: Joi.object().keys({
                campaign_index: Joi.string().required(),
                category_index: Joi.string().required()
            }),
            payload: Joi.alternatives().try([
                Joi.array().label('Campaign').items(CampaignValidation.postValidationSchema()),
                Joi.object().label('Campaign').keys(CampaignValidation.postValidationSchema())
            ])
        },
        description: 'Save Campaigns and associated Categories',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'ADD'
                }
            }
        }
    },
    update: {
        handler: async (request, reply) => {

            const connection = request.server.pandaEsOrm.connection;
            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const campaignModel = pandaEsOrm.getModel('CampaignModel');
            const categoryModel = pandaEsOrm.getModel('CategoryModel');
            const campaign = request.payload;

            try {
                if (campaign.is_main) {
                    await pandaEs.query.execute(connection, 'updateByQuery', {
                        index: campaignModel.index,
                        body: {
                            script: {
                                inline: "ctx._source.is_main = false"
                            },
                            query: {
                                match_all: {}
                            }
                        }
                    });
                }

                const existingCampaign = await campaignModel.findOneById(campaign.id);
                if (!_.isEmpty(existingCampaign.data)) {
                    //delete campaign
                    campaignModel.data = { id: campaign.id };
                    await campaignModel.delete();
                }

                let categories = [];
                if (!_.isUndefined(campaign.categories)) {
                    categories = campaign.categories;
                    delete campaign.categories
                }

                //insert campaign
                campaignModel.data = campaign;
                await campaignModel.create();

                if (categories.length > 0) {
                    // delete categories
                    await pandaEs.query.execute(connection, 'deleteByQuery', {
                        index: categoryModel.index,
                        body: {
                            query: {
                                bool: {
                                    must: [
                                        {
                                            terms: {
                                                campaign_id: [campaign.id]
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    });

                    //insert categories
                    for (const category of categories) {
                        categoryModel.data = category;
                        await categoryModel.create();
                    }
                }
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }

            return { message: 'Updated' };
        },
        auth: 'im-auth',
        validate: {
            payload: Joi.object().label('Campaign').keys(CampaignValidation.postValidationSchema())
        },
        description: 'Update Campaign',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'EDIT'
                }
            }
        }
    },
    bulkDeleteCampaignsAndAssociatedCategories: {
        handler: async (request, reply) => {

            const campaignsId = request.payload.map((itm) => itm.id);
            const connection = request.server.pandaEsOrm.connection;
            const pandaEs = request.server.pandaEs;
            const pandaEsOrm = request.server.pandaEsOrm;
            const campaignModel = pandaEsOrm.getModel('CampaignModel');

            try {
                // delete campaigns
                await pandaEs.query.execute(connection, 'deleteByQuery', {
                    index: campaignModel.index,
                    body: {
                        query: {
                            constant_score: {
                                filter: {
                                    terms: {
                                        id: campaignsId
                                    }
                                }
                            }
                        }
                    }
                });

                return { message: 'Deleted' };
            }
            catch (err) {
                return Boom.internal(err || 'Internal error');
            }
        },
        auth: 'im-auth',
        validate: {
            payload: Joi.array().items(
                Joi.object().keys({
                    id: Joi.number().required()
                })
            )
        },
        description: 'Delete bulk Campaigns and associated Categories',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '401': {
                        'description': 'Unauthorized'
                    }
                }
            },
            'hapi-internal-bridge': {
                auth: {
                    role: 'ROLE_PROMO',
                    permission: 'DELETE'
                }
            }
        }
    }
});
