'use strict';

import Routes from './routes';
import InitModels from './models/init';

exports.plugin = {
    register: async (server, options) => {

        // load models
        await InitModels(server);

        // load routes
        await server.route(Routes(options));
    },
    pkg: require('./package.json')
};
