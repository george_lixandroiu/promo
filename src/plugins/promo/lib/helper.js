'use strict';

module.exports = {

    /**
     * getValidationSchema
     *
     * @param arr
     * @param currentLevel
     * @param stopAtLevel
     * @param parent
     *
     * @returns {Array}
     */
    constructCategoriesTree(arr, currentLevel, stopAtLevel, parent) {

        const toReturn = [];

        for (const index in arr) {
            if (arr[index].parent_id === parent) {
                const subcategories = module.exports.constructCategoriesTree(arr, currentLevel + 1, stopAtLevel, arr[index].id);

                if (subcategories.length) {
                    arr[index].subcategories = subcategories;
                }

                if (currentLevel <= stopAtLevel) {
                    toReturn.push(arr[index]);
                }
            }
        }
        return toReturn;
    },

    /**
     * processFilters
     *
     * @param filters
     * @param searchForFilters
     *
     * @returns {Object}
     */
    processFilters(filters, searchForFilters = []) {

        const toReturn = {
            conditions: []
        };

        filtersLoop:
        for (let filter of filters) {
            const split = filter.split(':');

            if (split[1].search('_')) { //transform array filters
                split[1] = split[1].split('__');
            }

            for(const searchForFilter of searchForFilters) {
                if (searchForFilter === split[0]) {
                    toReturn[searchForFilter] = split[1];
                    break filtersLoop;
                }
            }

            toReturn.conditions.push({
                terms: {
                    [split[0]]: split[1]
                }
            });
        }

        return toReturn;
    },

    /**
     * bulkOperation
     * 
     * @param server
     * @param connection
     * @param buffer
     */
    bulkOperation(server, connection, buffer) {
        
        connection.bulk({
            body: buffer
        }, (err, resp) => {
            if (err) {
                throw err;
            }
            server.log(['info'], 'Indexed ' + buffer.length + ' documents');
        });
    }
};
