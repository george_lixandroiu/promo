'use strict';

import _ from 'lodash';

const conditions = {
    campaignModel: [
        {
            term: {
                status: 1
            }
        }
    ],
    categoryModel: [
        {
            term: {
                status: 1
            }
        }
    ]
};


module.exports = {

    /**
     * getPredefinedFiltersForModel
     *
     * @returns {Array}
     */
    getPredefinedFiltersForModel(model) {

        if (_.isUndefined(conditions[model])) {
            throw 'Unknown model!';
        }

        return conditions[model];
    },

    /**
     * campaignIsActive
     *
     * @param campaign
     *
     * @returns {Boolean}
     */
    campaignIsActive(campaign) {

        if (
            _.isEmpty(campaign) ||
            !_.get(campaign, 'status', 0)
        ) {
            return false;
        }

        return true;
    },
};
