'use strict';

module.exports = function (settings) {

    const CampaignCtrl = require('./handlers/campaign')(settings);
    const CategoryCtrl = require('./handlers/category')(settings);
    const ProductCtrl = require('./handlers/product')(settings);

    return [
        {
            method: 'GET',
            path: '/campaigns/{id}',
            config: CampaignCtrl.getById
        },
        {
            method: 'GET',
            path: '/campaigns',
            config: CampaignCtrl.getByFilters
        },
        {
            method: 'GET',
            path: '/campaigns/{id}/categories',
            config: CampaignCtrl.getCategoriesByCampaignIdAndCategoryFilters
        },
        {
            method: 'POST',
            path: '/campaigns/{campaign_index}/{category_index}',
            config: CampaignCtrl.bulkSave
        },
        {
            method: 'PUT',
            path: '/campaigns',
            config: CampaignCtrl.update
        },
        {
            method: 'DELETE',
            path: '/campaigns',
            config: CampaignCtrl.bulkDeleteCampaignsAndAssociatedCategories
        },

        {
            method: 'GET',
            path: '/categories/{id}/products',
            config: CategoryCtrl.getByIdAndAttachAssociatedProducts
        },
        {
            method: 'POST',
            path: '/categories',
            config: CategoryCtrl.save
        },
        {
            method: 'PUT',
            path: '/categories',
            config: CategoryCtrl.update
        },
        {
            method: 'DELETE',
            path: '/categories/{id}',
            config: CategoryCtrl.deleteCategoryAndAssociatedProducts
        },
        {
            method: 'DELETE',
            path: '/categories',
            config: CategoryCtrl.deleteBulk
        },

        {
            method: 'GET',
            path: '/products/{sku}',
            config: ProductCtrl.getBySku
        },
        {
            method: 'GET',
            path: '/products',
            config: ProductCtrl.getByFilters
        },
        {
            method: 'POST',
            path: '/products/{index?}',
            config: ProductCtrl.bulkSave
        },
        {
            method: 'PUT',
            path: '/products',
            config: ProductCtrl.update
        },
        {
            method: 'DELETE',
            path: '/products',
            config: ProductCtrl.bulkDelete
        }
    ];
};
