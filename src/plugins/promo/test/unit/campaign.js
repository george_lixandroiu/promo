'use strict';

import Code from '@hapi/code';
import lab from '@hapi/lab';
import Hoek from 'hoek';
import _ from 'lodash';
import sleep from 'system-sleep';
import Helper from '../helper';
const pandaOrm = Helper.initiatePandaEsForTests();
const CampaignFixture = JSON.parse(JSON.stringify(import('../fixtures/campaign')));
const CategoryFixture = JSON.parse(JSON.stringify(import('../fixtures/category')));

const CampaignHandler = require('../../handlers/campaign')(Helper.loadOptions());

lab.experiment('Campaigns,', () => {

    lab.test('bulkSave -> should successfully save a bulk of campaigns', async (done) => {

        const campaign = JSON.parse(JSON.stringify(CampaignFixture));
        campaign.categories = CategoryFixture;

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm
            },
            payload: campaign
        };

        const response = await CampaignHandler.bulkSave.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.message).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('getById -> should successfully retrieve a campaign', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm,
                pandaEs: pandaOrm.pandaEs
            },
            params: {
                id: CampaignFixture.id
            }
        };

        const response = await CampaignHandler.getById.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.id).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('getByFilters -> should successfully retrieve a campaign', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm,
                pandaEs: pandaOrm.pandaEs
            },
            query: {
                filter: [
                    'name:' + CampaignFixture.name
                ]
            }
        };

        const response = await CampaignHandler.getByFilters.handler(request, {});

        Code.expect(response).to.be.an.array();

        sleep(1000);

        done;
    });

    lab.test('getCategoriesByCampaignIdAndCategoryFilters -> should successfully retrieve a campaign and associated categories', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm,
                pandaEs: pandaOrm.pandaEs
            },
            params: {
                id: CampaignFixture.id
            },
            query: {
                filter: [
                    'level:2'
                ]
            }
        };

        const response = await CampaignHandler.getCategoriesByCampaignIdAndCategoryFilters.handler(request, {});

        Code.expect(response).to.be.an.array();

        try {
            Code.expect(response[0].subcategories).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('bulkDeleteCampaignsAndAssociatedCategories -> should successfully delete campaigns and associated categories', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm,
                pandaEs: pandaOrm.pandaEs
            },
            payload: [
                {
                    id: CampaignFixture.id
                }
            ]
        };

        const response = await CampaignHandler.bulkDeleteCampaignsAndAssociatedCategories.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.message).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });
});
