'use strict';

import Code from '@hapi/code';
import lab from '@hapi/lab';
import Hoek from 'hoek';
import _ from 'lodash';
import sleep from 'system-sleep';
import Helper from '../helper';
const ProductFixture = JSON.parse(JSON.stringify(import('../fixtures/product')));
const ProductHandler = import('../../handlers/product')(Helper.loadOptions());
const pandaOrm = Helper.initiatePandaEsForTests();

lab.experiment('Products,', () => {

    lab.test('bulkSave -> should successfully save a product', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm
            },
            payload: JSON.parse(JSON.stringify(ProductFixture[0]))
        };

        const response = await ProductHandler.bulkSave.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.message).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('update -> should successfully update a product', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm
            },
            payload: JSON.parse(JSON.stringify(ProductFixture[0]))
        };

        const response = await ProductHandler.update.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.message).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('getBySku -> should successfully get a product', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm
            },
            params: {
                sku: ProductFixture[0].sku
            }
        };

        const response = await ProductHandler.getBySku.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.id).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('getByFilters -> should successfully get a product', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm,
                pandaEs: pandaOrm.pandaEs
            },
            query: {
                filter: [
                    'stock_status:' + ProductFixture[0].stock_status
                ]
            }
        };

        const response = await ProductHandler.getByFilters.handler(request, {});

        Code.expect(response).to.be.an.array();

        try {
            Code.expect(response[0].id).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('bulkDelete -> should successfully delete a product', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm,
                pandaEs: pandaOrm.pandaEs
            },
            payload: [
                {
                    id: ProductFixture[0].id
                }
            ]
        };

        const response = await ProductHandler.bulkDelete.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.message).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });
});
