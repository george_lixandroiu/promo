'use strict';

import Code from '@hapi/code';
import lab from '@hapi/lab';
import Hoek from 'hoek';
import _ from 'lodash';
import sleep from 'system-sleep';
import Helper from '../helper';
const pandaOrm = Helper.initiatePandaEsForTests();
const CategoryFixture = JSON.parse(JSON.stringify(require('../fixtures/category')));
const CampaignFixture = JSON.parse(JSON.stringify(require('../fixtures/campaign')));

const CategoryHandler = import('../../handlers/category')(Helper.loadOptions());
const CampaignHandler = import('../../handlers/campaign')(Helper.loadOptions());

lab.experiment('Categories,', () => {

    lab.before(async (done) => { //need a campaign as a dependency for category save

        const campaignSaveRequest = {
            server: {
                pandaEsOrm: pandaOrm.orm
            },
            payload: CampaignFixture
        };

        await CampaignHandler.bulkSave.handler(campaignSaveRequest, {});

        sleep(1000);

        done;
    });

    lab.test('save -> should successfully save a category', async (done) => {

        const categorySaveRequest = {
            server: {
                pandaEsOrm: pandaOrm.orm
            },
            payload: JSON.parse(JSON.stringify(CategoryFixture[0]))
        };

        const response = await CategoryHandler.save.handler(categorySaveRequest, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.message).to.exists();
            Code.expect(response.message).to.equal('Saved');
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('update -> should successfully update a category', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm
            },
            payload: JSON.parse(JSON.stringify(CategoryFixture[0]))
        };

        const response = await CategoryHandler.update.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.message).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('getByIdAndAttachAssociatedProducts -> should successfully retrieve a category', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm,
                pandaEs: pandaOrm.pandaEs
            },
            params: {
                id: CategoryFixture[0].id
            }
        };

        const response = await CategoryHandler.getByIdAndAttachAssociatedProducts.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.id).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.test('deleteCategoryAndAssociatedProducts -> should successfully delete a category', async (done) => {

        const request = {
            server: {
                pandaEsOrm: pandaOrm.orm,
                pandaEs: pandaOrm.pandaEs
            },
            params: {
                id: CategoryFixture[0].id
            }
        };

        const response = await CategoryHandler.deleteCategoryAndAssociatedProducts.handler(request, {});

        Code.expect(response).to.be.an.object();

        try {
            Code.expect(response.message).to.exists();
        }
        catch (err) {
            Hoek.assert(_.isEqual(err.message), 'Invalid response!');
        }

        sleep(1000);

        done;
    });

    lab.after(async (done) => {

        //delete campaign
        const campaignDeleteRequest = {
            server: {
                pandaEsOrm: pandaOrm.orm,
                pandaEs: pandaOrm.pandaEs
            },
            payload: [
                {
                    id: CategoryFixture[0].campaign_id
                }
            ]
        };

        await CampaignHandler.bulkDeleteCampaignsAndAssociatedCategories.handler(campaignDeleteRequest, {});

        sleep(1000);

        done;
    });
});
