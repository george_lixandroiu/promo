'use strict';

import plugin from 'panda-es-orm';

const models = [
    require('../models/product'),
    require('../models/campaign'),
    require('../models/category')
];

module.exports = {

    /**
     * initiatePandaEsForTests
     *
     * @returns {Object}
     */
    initiatePandaEsForTests() {

        const toReturn = {};

        const esOptions = {
            connection: {
                host: 'localhost:9200',
                requestTimeout: 2000
            },
            logger: {
                level: 10
            }
        };

        toReturn.orm = new plugin.orm(esOptions);
        toReturn.pandaEs = plugin;

        module.exports.attachModels();

        return toReturn;
    },

    /**
     * attachModels
     */
    attachModels() {

        models.forEach((model) => {

            new plugin.model(
                model.name,
                model.index,
                model.idKey,
                model.validation
            );
        });
    },

    /**
     * attachModels
     *
     * @returns {Object}
     */
    loadOptions() {

        return {
            categories: {
                maxLevel: 3,
                allowedFilters: ['level']
            },
            campaigns: {
                allowedFilters: ['name']
            },
            products: {
                allowedFilters: ['stock_status']
            }
        };
    }
};
