'use strict';

module.exports = {
    name: 'Test Campaign 1',
    slug: 'slug_for_test_campaign_1',
    status: 1,
    is_main: true,
    from_date: new Date().toISOString().slice(0, 10),
    to_date: new Date().toISOString().slice(0, 10),
    updated_at: new Date().toISOString().slice(0, 10),
    created_at: new Date().toISOString().slice(0, 10),
    currency: 'lei',
    id: 100
};
