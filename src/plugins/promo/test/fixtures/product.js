'use strict';

module.exports = [
    {
        entity_id: 301,
        id: 300,
        image: 'image url',
        stock_status: 1,
        name: 'Test product 1',
        url: 'url',
        price: 10,
        final_price: 10,
        sku: 'sku123',
        discount_type: 'by_fixed'
    }
];
