'use strict';

module.exports = [
    {
        name: 'Test Category 1',
        slug: 'slug_for_test_category_1',
        status: 1,
        sort_order: 1,
        campaign_id: 100,
        campaign_name: 'Test Campaign 1',
        id: 200
    },
    {
        name: 'Test Category 2',
        slug: 'slug_for_test_category_2',
        status: 1,
        sort_order: 1,
        campaign_id: 100,
        campaign_name: 'Test Campaign 1',
        id: 201,
        parent_id: 200
    }
];
