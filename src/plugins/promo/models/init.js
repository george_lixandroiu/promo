'use strict';

const definedModels = [
    require('./product'),
    require('./campaign'),
    require('./category')
];

module.exports = async (server) => {
    
    const pandaEs = server.pandaEs;
    const models = {};

    definedModels.forEach((modelOptions) => {

        models[modelOptions.name] = new pandaEs.model({
            name: modelOptions.name,
            index: modelOptions.index,
            idKey: modelOptions.idKey,
            validationSchema: modelOptions.validation
        });

        if (modelOptions.listeners) {
            Object.keys(modelOptions.listeners).forEach((evtId) => {

                const evtName = modelOptions.name.toLowerCase() + '_' + evtId;
                models[modelOptions.name].on(evtName, modelOptions.listeners[evtId]);
            });
        }
    });
};