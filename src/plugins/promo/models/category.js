'use strict';

import Validation from '../validate/category';

module.exports = {

    name: 'CategoryModel',
    index: 'promo_categories',
    idKey: 'id',
    validation: Validation.getValidationSchema()
};
