'use strict';

import Validation from '../validate/product';

module.exports = {

    name: 'ProductModel',
    index: 'promo_products',
    idKey: 'sku',
    validation: Validation.getValidationSchema()
};
