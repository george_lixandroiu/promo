'use strict';

import Validation from '../validate/campaign';

module.exports = {

    name: 'CampaignModel',
    index: 'promo_campaigns',
    idKey: 'id',
    validation: Validation.getValidationSchema()
};
