'use strict';

import Joi from '@hapi/joi';
import ProductValidation from './product';

module.exports = {
    /**
     * getValidationSchema
     *
     * @returns {*}
     */
    getValidationSchema: () => {

        return {
            name: Joi.string().required(),
            slug: Joi.string().required(),
            status: Joi.number().required(),
            type: Joi.number().optional(),
            properties: Joi.object().optional(),
            parent_id: Joi.number().optional(),
            sort_order: Joi.number().required(),
            campaign_id: Joi.number().required(),
            product_ids: Joi.array().optional(),
            campaign_name: Joi.string().required(),
            product_count: Joi.number().optional(),
            id: Joi.number().required()
        };
    },

    /**
     * getValidationSchemaForResponseWithMultipleLevels
     *
     * @returns {*}
     */
    getValidationSchemaForResponseWithMultipleLevels: () => {

        const response = (module.exports.getValidationSchema());

        response.subcategories = Joi.array().optional();

        return response;
    },

    /**
     * getValidationSchemaForResponseWithProducts
     *
     * @returns {*}
     */
    getValidationSchemaForResponseWithProducts: () => {

        const response = (module.exports.getValidationSchema());

        response.products = Joi.array().label('Categories').items(ProductValidation.getValidationSchema());

        return response;
    }
};
