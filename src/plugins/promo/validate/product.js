'use strict';

import Joi from '@hapi/joi';

module.exports = {
    /**
     * getValidationSchema
     *
     * @returns {*}
     */
    getValidationSchema: () => {

        return {
            entity_id: Joi.number().required(),
            id: Joi.number().required(),
            image: Joi.string().required(),
            stock_status: Joi.number().default(0),
            name: Joi.string().required(),
            url: Joi.string().required(),
            price: Joi.number().required(),
            final_price: Joi.number().required(),
            sku: Joi.string().required(),
            best_buy: Joi.number().optional(),
            discount_type: Joi.string().required(),
            discount_amount: Joi.number().required(),
            labels: Joi.object().keys({
                gift: Joi.number().optional(),
                package: Joi.number().optional(),
                in_store: Joi.number().optional(),
                free_shipping: Joi.number().optional(),
                voucher: Joi.string().optional()
            }).optional()
        };
    }
};
