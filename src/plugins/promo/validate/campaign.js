'use strict';

import BaseJoi from '@hapi/joi';
import Extension from '@hapi/joi-date';
import CategoryValidation from './category';
const Joi = BaseJoi.extend(Extension);

module.exports = {
    /**
     * getValidationSchema
     *
     * @returns {*}
     */
    getValidationSchema: () => {

        return {
            name: Joi.string().required(),
            slug: Joi.string().required(),
            properties: Joi.object().optional(),
            status: Joi.number().required(),
            is_main: Joi.boolean().optional(),
            from_date: Joi.date().format(['YYYY-MM-DD', 'YYYY-MM-DD HH:mm:ss']).raw().optional().allow(null),
            to_date: Joi.date().format(['YYYY-MM-DD', 'YYYY-MM-DD HH:mm:ss']).raw().optional().allow(null),
            updated_at: Joi.date().format(['YYYY-MM-DD', 'YYYY-MM-DD HH:mm:ss']).raw().required(),
            created_at: Joi.date().format(['YYYY-MM-DD', 'YYYY-MM-DD HH:mm:ss']).raw().required(),
            currency: Joi.string().required(),
            banner: Joi.string().optional(),
            id: Joi.number().required(),
            display_type: Joi.number().optional()
        };
    },

    /**
     * postValidationSchema
     *
     * @returns {*}
     */
    postValidationSchema: () => {

        const response = (module.exports.getValidationSchema());

        response.categories = Joi.array().label('Categories').items(
            CategoryValidation.getValidationSchema()
        ).optional();

        return response;
    }
};
