'use strict';

import Joi from '@hapi/joi';

module.exports = {
    /**
     * getFiltersValidationSchema
     *
     * @param allowedFilters
     *
     * @returns {*}
     */
    getFiltersValidationSchema: (allowedFilters) => {

        const keyMapping = allowedFilters.join('|');

        return Joi.string().regex(new RegExp('(' + keyMapping + '):[a-zA-Z0-9 \-\_\/\?]{1,500}'));
    }
};
